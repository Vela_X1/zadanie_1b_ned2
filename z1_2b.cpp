#include <iostream>

using namespace std;

#include <iostream>

int main() {
int month;
cout << "Введите номер месяца: ";
cin >> month;

int days;
switch (month) {
case 1:
case 3:
case 5:
case 7:
case 8:
case 10:
case 12:
days = 31;
break;
case 4:
case 6:
case 9:
case 11:
days = 30;
break;
case 2:
int year;
cout << "Введите год: ";
cin >> year;

if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
days = 29; // високосный год
} else {
days = 28; // обычный год
}
break;
default:
cout << "Некорректный номер месяца!" <<endl;
return 1;
}

cout << "Количество дней: " << days << endl;

return 0;
}
